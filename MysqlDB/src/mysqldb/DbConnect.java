
package mysqldb;

import java.sql.Statement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;


public class DbConnect {
    private Connection con;
    private Statement st;
    private ResultSet rs;
    
    public DbConnect(){
        try{
            Class.forName("com.mysql.jdbc.Driver");
            
            con = DriverManager.getConnection("jdbc:mysql://localhost:3306/toiwo","toiwo","lollero");
            st = con.createStatement();
            
    }catch(Exception ex){
            System.out.println("Error: " + ex);
    }
             
     }
    public void getData(){
        try{
            String query = "select * from ottomaatti";
            rs = st.executeQuery(query);
            System.out.println("Records from Database:");
            while(rs.next()){
                String osoite = rs.getString("Kohteen_osoite");
                String postinumero = rs.getString("postinumero");
                System.out.println("Osoite: "+osoite+" "+"postinumero: "+postinumero);
            }
                    
        }catch(Exception ex){
            System.out.println("Error: "+ex);
        }
    }
}
