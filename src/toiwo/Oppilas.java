/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package toiwo;

/**
 *
 * @author Hannu
 */
public class Oppilas {
    
    private int id;
    private String etunimi;
    private String sukunimi;
    private int suoritukset;
    
    public Oppilas(){
        id = 0;
        etunimi = "";
        sukunimi = "";
        suoritukset = 0;
    }
    
    public Oppilas(int id, String etunimi, String sukunimi, int suoritukset){
        this.id = id;
        this.etunimi = etunimi;         
        this.sukunimi = sukunimi;
        this.suoritukset = suoritukset;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getEtunimi() {
        return etunimi;
    }

    public void setEtunimi(String etunimi) {
        this.etunimi = etunimi;
    }

    public String getSukunimi() {
        return sukunimi;
    }

    public void setSukunimi(String sukunimi) {
        this.sukunimi = sukunimi;
    }
    
    public void setSuoritukset(int suoritukset){
        this.suoritukset = suoritukset;
    }
    
    public int getSuoritukset(){
        return suoritukset;
    }
    
    
    
}
