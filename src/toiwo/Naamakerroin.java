/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package toiwo;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import javafx.scene.image.Image;
import javax.imageio.ImageIO;

/**
 *
 * @author Hannu
 */
public class Naamakerroin {
    
    private int oppilas_id;
    private int hymiö_id;
    
    
    public Naamakerroin(){
        oppilas_id = 0;
        hymiö_id = 0;
    }
    
    public Naamakerroin(int oppilas_id, int hymiö_id){
        this.oppilas_id = oppilas_id;
        this.hymiö_id = hymiö_id;
    }
    
    public void setOppilas_id(int oppilas_id){
        this.oppilas_id = oppilas_id;
    }
    
    public void setHymiö_id(int hymiö_id){
        this.hymiö_id = hymiö_id;
    }
    
    public int getOppilas_id(){
        return oppilas_id;
    }
    
    public int getHymiö_id(){
        return hymiö_id;
    }
    
}
