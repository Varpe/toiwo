/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package toiwo;

import java.util.ArrayList;

/**
 *
 * @author Hannu
 */
public class Controller { // Luokka sisältää metodit jotka välittävät tietoa gui:n ja tietokannan välillä
    
    private Main gui;
    private DataBaseConnector tietokanta;
    private DisplayConfirmation displayPopup;
    private Grafiikka grafiikka;
    // Controllerin konstruktori, saa parametrikseen guin, tietokannan ja popup-luokan
    Controller(Main gui, DataBaseConnector tietokanta, DisplayConfirmation displayPopup, Grafiikka grafiikka) {
        this.gui = gui;
        this.tietokanta = tietokanta;
        this.displayPopup = displayPopup;
        this.grafiikka = grafiikka;
    }
    
    // Metodi jolla luodaan yhteys mysql tietokantaan
    // palauttaa true jos onnistua, false jos ei onnistunut
    public boolean connectToTietokanta(){
        return tietokanta.Connect(); // yhdistetään tietokanta luokan kautta tietokantaan
    }
    
    // Metodi jolla luodaan uusi oppilas mysql tietokantaan
    // ottaa parametriksi uuden oppilaan tiedot
    // palauttaa guihin true jos oppilaan luominen onnistua, false jos ei onnistunut
    public boolean createOppilas(int oppilas_id, String etunimi, String sukunimi, int suoritukset){
        return tietokanta.createOppilas(oppilas_id, etunimi, sukunimi, suoritukset); // välitetään tiedot gui:lta tietokanta luokkaan
    }
    
    // Metodi jolla luodaan uusi kurssi mysql tietokantaan
    // ottaa parametriksi kurssin tiedot
    // palauttaa  guihin true jos kurssin luominen onnistui, false jos ei onnistunut
    public boolean createKurssi(String nimi,int kurssin_id, String kuvaus){
        return tietokanta.createKurssi(nimi, kurssin_id, kuvaus); // välitetään tiedot gui:lta tietokanta luokkaan
    }
    
    // Metodi jolla näytetään popup kun tallennetaan/poistetaa
    // ottaa parametriksi boolean tilan, true kertoo että tallentaminen/poistaminen onnistui ja false epäonnistumista
    public void näytäPopup(boolean tila, String missä){
        displayPopup.display(tila, missä); // välitetään tiedot popup luokalle
    }
    
    // Metodi palauttaa gui:hin tietokannasta haetut oppilaat Oppilas olio listassa
    public ArrayList<Oppilas> haeKaikkiOppilaat(){
        return tietokanta.haeKaikkiOppilaat(); // palautetaan Oppilas lista gui:lle
    }
    
    // Metodi palauttaa gui:hin tietokannasta haetut kurssit Kurssi olio listassa
    public ArrayList<Kurssi> haeKaikkiKurssit(){
        return tietokanta.haeKaikkiKurssit(); // palautetaan Kurssi lista gui:lle
    }
    
    // Metodi välittää parametriksi saadun poistettavan oppilaan id:n tietokanta luokkaan
    // palauttaa gui:lle true jos oppilaan poisto onnistui, false jos epäonnistui
    public boolean poistaOppilas(int oppilas_id){
        return tietokanta.poistaOppilas(oppilas_id); // välitetään oppilaan id tietokanta luokkaan ja palautetaan tulos gui:lle
    }
    
    // Metodi välittää parametriksi saadun poistettava kurssin id:n tietokanta luokkaan
    // palauttaa gui:lle true jos kurssin poistaminen onnistui, false jos epäonnistui
    public boolean poistaKurssi(int kurssin_id){
        return tietokanta.poistaKurssi(kurssin_id); // välitetään kurssin id tietokanta luokkan ja palautetaan tulos gui:lle
    }
    
    // TESTATTAVA JA LUOTAVA GUIHIN TARVITTAVAT JUTSKAT
    public boolean updateOppilas(int oppilas_id, String etunimi, String sukunimi, int suoritukset){
        return tietokanta.updateOppilas(oppilas_id, etunimi, sukunimi, suoritukset);
    }
    
    // TESTATTAVA JA LUOTAVA GUIHINI TARVITTAVAT JUTSKAT
    public boolean updateKurssi(int kurssin_id, String KurssinNimi, String kurssinKuvaus){
        return tietokanta.updateKurssi(kurssin_id, KurssinNimi, kurssinKuvaus);
    }
    
    // Luodaan oppilas_kurssi olio ja palautetaan true jos luonti onnistui, false jos ei onnistunut
    public boolean cretaOppilasKurssi(int oppilas_id, int kurssin_id){
        return tietokanta.creataOppilasKurssi(oppilas_id, kurssin_id);
    }
    
    // Metodi hakee kaikki oppilas_kurssi oliot ja palauttaa ne OppilasKurssi listassa
    public ArrayList<OppilasKurssi> haeKaikkiOppilasKurssi(){
        return tietokanta.HaeKaikkiOppilasKurssi();
    }
    
    // Metodi hakee kaikki oppilaat tietyltä kurssilta
    // Palauttaa listan Oppilas olioita jotka ovat kurssilla jonka kurssi_id on parametrinä
    public ArrayList<Oppilas> haeKaikkiOppilaatKurssilta(int kurssin_id){
        return tietokanta.OppilasKurssiOppilaaksi(tietokanta.HaeKaikkiOppilaatKurssilta(kurssin_id));
    }
    
    // Metodi hakee kaikki kurssit joilla oppilas on
    // Palauttaa listan Kurssi olioita joilla oppilas on jonka oppilas_id on parametrinä
    public ArrayList<Kurssi> haeKaikkiOppilaanKurssit(int oppilas_id){
        return tietokanta.OppilasKurssiKurssiksi(tietokanta.HaeKaikkiOppilaanKurssit(oppilas_id));
    }
    
    // Metodi poistaa oppilaan kurssilta 
    // Tunnistetaan poistettava oppilas parametrinä saatavana oppilas_id:llä
    // Tunnistetaan kurssi jolta oppilas pitää poista parametrinä saatavana kurssi_id:llä
    // Palauttaa true jos poisto onnistui, false jos poisto epäonnistui
    public boolean poistaOppilasKurssilta(int oppilas_id, int kurssin_id){
        return tietokanta.PoistaOppilasKurssilta(oppilas_id, kurssin_id);
    }
    
    
    // Metodi asettaa oppilaalle naamakertoimen
    // Oppilas tunnistetaan parametrinä saatavalla oppilas_id:llä
    // Naamakerroin tunnistetaan parametrinä saatavalla hymiö_id:llä
    // Palautetaan true jos asetus onnistui, false jos asetus epäonnistui
    public boolean asetaNaamakerroin(int oppilas_id, int hymiö_id){
        return tietokanta.AsetaNaamakerroin(oppilas_id, hymiö_id);
    }
    
    // Metodi hakee yksittäisen oppilaan naamakertoimen
    // Oppilas tunnistetaaan parametrinä saatavalla oppilas_id:llä
    // Palauttaa naamakerroin olio listan
    public ArrayList<Naamakerroin> haeNaamakerroin(int oppilas_id){
        return tietokanta.HaeNaamakerroin(oppilas_id);
    }
    
    // Metodi hakee koko kurssin oppilaiden naamakertoimet
    // Kurssi tunnistetaan OppilasKurssiListasta saatavalta kurssin_id:llä
    // OppilasKurssiLista sisältää jokaisen oppilaan oppilas_id:n jolla saadaan haettua naamakerroin
    // Palauttaa naamakerroin olio listan kaikista kurssilla olevista opiskelijoista
    public ArrayList<Naamakerroin> haeNaamakerroin(ArrayList<OppilasKurssi> oppilasKurssiLista){
        return tietokanta.HaeNaamakerroin(oppilasKurssiLista);
    }
    
    // Metodi hakee jokaisen oppilaan naamakertoimen
    // Palauttaa naamakerroin olio listan jokaisen oppilaan naamakertoimesta
    public ArrayList<Naamakerroin> haeKaikkiNaamakertoimet(){
        return tietokanta.HaeKaikkiNaamakertoimet();
    }
    
    // Metodi päivittää oppilaan naamakerrointa
    // Oppilas tunnistetaan parametrinä saadulla oppilas_id:llä
    // Naamakerroin päivitetään parametrillä hymiö_id
    // Palauttaa true jos päivitys onnistui, false jos epäonnistui
    public boolean updateNaamakerroin(int oppilas_id, int hymiö_id){
        return tietokanta.updateNaamakerroin(oppilas_id, hymiö_id);
    }
    
    // Metodi antaa oppilaalle tietyltä kurssilta arvosanan
    // Oppilas ja kurssi tunnistetaan parametreillä oppilas_id ja kurssin_id
    // Parametri arvosana määrittää annettavan arvosanan
    // Palauttaa true jos arvosanan antaminen onnistui, false jos se epäonnistui
    public boolean createArvosana(int oppilas_id, int kurssin_id, int arvosana){
        return tietokanta.createArvosana(oppilas_id, kurssin_id, arvosana);
    }
    
    // Metodi päivittää oppilaan arvosanaa tietyllä kurssilla
    // Kurssi ja oppilas tunnistetaan parametreillä kurssin_id ja oppilas_id
    // Vain arvosanaa voi päivittää
    // Palautetaan true jos arvosanan päivittäminen onnistui, false jos ei onnistunut
    public boolean updateArvosana(int oppilas_id, int kurssin_id, int arvosana){
        return tietokanta.updateArvosana(oppilas_id, kurssin_id, arvosana);
    }
    
    // Metodi hakee yhden oppilaan arvosanan tietyltä kurssilta
    // Kurssi ja oppilas tunnistetaan parametrinä saaduilla oppilas_id ja kurssin_id
    // Palautetaan lista arvosanoista
    public Arvosana HaeArvosana(int oppilas_id, int kurssin_id){
        return tietokanta.HaeArvosana(oppilas_id, kurssin_id);
    }
    
    // Metodi hakee yhden kurssin jokaisen opiskelijan arvosanat
    // Kurssi jolta arvosanat haetaan tunnistetaan parametrinä saadulla kurssin_id:llä
    // Palauttaa listan arvosana olioita
    public ArrayList<Arvosana> HaeArvosana(int kurssin_id){
        return tietokanta.HaeArvosana(kurssin_id);
    }
    
    // Metodi hakee yhden oppilaan kaikki arvosanat jokaiselta kurssilta
    // Oppilas tunnistetaan parametrillä oppilas_id
    // Palautetaan lista kaikista oppilaan arvosanoista     
    public ArrayList<Arvosana> HaeOppilaanKaikkiArvosanat(int oppilas_id){
        return tietokanta.HaeOppilaanKaikkiArvosanat(oppilas_id);
    }
}
