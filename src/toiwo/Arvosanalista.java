/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package toiwo;

import java.awt.event.ActionListener;
import java.util.ArrayList;
import javafx.application.Application;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventType;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TablePosition;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.effect.DropShadowBuilder;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import javafx.util.Callback;

/**
 *
 * @author Narayan
 */

public class Arvosanalista extends Application{

 /*   ObservableList<String> oppilaat =FXCollections.observableArrayList();
            for(int i = 0; i < lista.size(); i++){
                String tallenne = "Id: " + lista.get(i).getId() + " Etunimi: " + lista.get(i).getEtunimi() + " Sukunimi: " + lista.get(i).getSukunimi();
                oppilaat.add(tallenne);
            }
    
   */ 
    
    private int tyhmä = 0;
    private ChoiceBox choice;
DataBaseConnector databaseconnector; 
    //Sample Values
    public void init(){
    databaseconnector = new DataBaseConnector();
    databaseconnector.Connect();
    }


    final ObservableList<Integer> arvosanat = FXCollections.observableArrayList(4,5,6,7,8,9,10);
    
    public void lataaTaulut(){
    ArrayList<Oppilas> lista = databaseconnector.haeKaikkiOppilaat();
    
    ObservableList<String> etunimet = FXCollections.observableArrayList();
            for(int i = 0; i<lista.size();i++){
            String tallenne = lista.get(i).getEtunimi();
            etunimet.add(tallenne);
}
            ObservableList<String> sukunimet = FXCollections.observableArrayList();
            for(int i = 0;i<lista.size();i++){
                String tallenne = lista.get(i).getSukunimi();
                sukunimet.add(tallenne);
            }
            ObservableList<String> idt = FXCollections.observableArrayList();
            for(int i=0;i<lista.size();i++){
                String tallenne = Integer.toString(lista.get(i).getId());
                idt.add(tallenne);
            }
            
            /*ObservableList<Integer> arvot = FXCollections.observableArrayList();
            for(int i = 0; i < lista.size(); i++){
                arvosana = databaseconnector.HaeArvosana(lista.get(i).getId(), 1);
                int ArvoSana = arvosana.getArvosana();
                arvot.add(ArvoSana);
            }*/
            
            
    }
    
  
    
    
    
    
    
    
    
    
    /**
     * This function gives the full configured TableView 
     * @return TableView
     */
    public TableView getTableView(){
        TableView<OppilasArvosana> table = new TableView<OppilasArvosana>(); 
        table.setTableMenuButtonVisible(true);
        
        /*
         * Creating the TableColumn for the TableView
         * The property value Factory name must match with the 
         * Generic Class's(Music's) property
         */
        TableColumn<OppilasArvosana, Integer> OppilasID = new TableColumn<OppilasArvosana, Integer>("Oppilas ID");
        OppilasID.setCellValueFactory(new PropertyValueFactory("oppilas_id"));
        OppilasID.setPrefWidth(100); 
        
        TableColumn<OppilasArvosana ,Integer> KurssinID = new TableColumn<OppilasArvosana,Integer>("Kurssin ID");
        KurssinID.setCellValueFactory(new PropertyValueFactory("kurssin_id"));
        KurssinID.setPrefWidth(100); 
         
        TableColumn<OppilasArvosana, Integer> Suoritukset = new TableColumn<OppilasArvosana,Integer>("Suoritukset");
        Suoritukset.setCellValueFactory(new PropertyValueFactory("suoritukset"));
        Suoritukset.setPrefWidth(100); 
        
        TableColumn<OppilasArvosana ,Integer> Arvosana = new TableColumn<OppilasArvosana,Integer>("Arvosana");
        Arvosana.setCellValueFactory(new PropertyValueFactory("arvosana"));
        Arvosana.setPrefWidth(100); 
        
        TableColumn<OppilasArvosana ,String> Etunimi = new TableColumn<OppilasArvosana,String>("Etunimi");
        Etunimi.setCellValueFactory(new PropertyValueFactory("etunimi"));
        Etunimi.setPrefWidth(200); 
        
        TableColumn<OppilasArvosana ,String> Sukunimi = new TableColumn<OppilasArvosana,String>("Sukunimi");
        Sukunimi.setCellValueFactory(new PropertyValueFactory("sukunimi"));
        Sukunimi.setPrefWidth(200); 
        
        ArrayList<Oppilas> oppilasLista = databaseconnector.haeKaikkiOppilaat();
        
        // SETTING THE CELL FACTORY FOR THE RATINGS COLUMN         
        Arvosana.setCellFactory(new Callback<TableColumn<OppilasArvosana,Integer>,TableCell<OppilasArvosana,Integer>>(){        
            @Override
            public TableCell<OppilasArvosana, Integer> call(TableColumn<OppilasArvosana, Integer> param) {                
                TableCell<OppilasArvosana, Integer> cell = new TableCell<OppilasArvosana, Integer>(){
                    @Override
                    public void updateItem(Integer item, boolean empty) {
                        if(item!=null){
                            
                           choice = new ChoiceBox(arvosanat);                                                      
                           choice.getSelectionModel().select(arvosanat.indexOf(item));
                           //SETTING ALL THE GRAPHICS COMPONENT FOR CELL
                           setGraphic(choice);
                           //Hae oppilaan id, kurssin id ja arvosana
                        choice.getSelectionModel().selectedIndexProperty().addListener(new ChangeListener<Number>(){
                         @Override
                         public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
                             System.out.println("Aloitetaan vaihto!");
                             OppilasArvosana haettuOppilas  = table.getSelectionModel().getSelectedItem();
                             int oppilasId = haettuOppilas.getOppilas_id();
                             int kurssinId = haettuOppilas.getKurssin_id();
                             int uusiArvosana = arvosanat.indexOf(item);
                             System.out.println("OppilasId: " + oppilasId + ", kurssinId: " + kurssinId + " ja arvosana: " + uusiArvosana);
                             databaseconnector.updateArvosana(oppilasId, kurssinId, uusiArvosana);
                         }
                     });
                    
                        } 
                    }
                };                           
                return cell;
            }
            
        });        
        
       OppilasArvosana oppilasArvosana;
        //ADDING ALL THE COLUMNS TO TABLEVIEW
        table.getColumns().addAll(OppilasID, KurssinID, Suoritukset, Arvosana, Etunimi, Sukunimi);        
        
        ObservableList<OppilasArvosana> oppilaat = FXCollections.observableArrayList();
        for(int i = 0; i < oppilasLista.size(); i++){
            Arvosana arvo = databaseconnector.HaeArvosana(oppilasLista.get(i).getId(), 1);
            System.out.println(arvo);
            int arvosana = arvo.getArvosana();
            int kurssin_id = arvo.getKurssin_id();
            int oppilas_id = oppilasLista.get(i).getId();
            //int kurssin_id = databaseconnector.HaeArvosana(oppilasLista.get(i).getId(), 1).getArvosana();
            int suoritukset = oppilasLista.get(i).getSuoritukset();
            //int arvosana = databaseconnector.HaeArvosana(oppilasLista.get(i).getId(), 1).getArvosana();
            String etunimi = oppilasLista.get(i).getEtunimi();
            String sukunimi = oppilasLista.get(i).getSukunimi();
            oppilasArvosana = new OppilasArvosana(oppilas_id, kurssin_id, suoritukset, arvosana, etunimi, sukunimi);
            System.out.println(oppilasArvosana);
            oppilaat.add(oppilasArvosana);
        }
        
        
        table.setItems(oppilaat);
       /* //ADDING ROWS INTO TABLEVIEW
        ObservableList<Oppilas> oppilaat = FXCollections.observableArrayList();
        for(int i = 0; i<databaseconnector.haeKaikkiOppilaat().size(); i++){
            Oppilas et = (databaseconnector.haeKaikkiOppilaat().get(i));
            Oppilas su = (databaseconnector.haeKaikkiOppilaat().get(i)); 
            oppilaat.add(su); 
        }        
        table.setItems(oppilaat); 
        */
        return table;
    }
    
    
    /**
     * This function gives the fancy Background for the application
     * @return 
     */
    public Group getBackground(){
        Group group = new Group(); 
        group.setLayoutX(40); 
        group.setLayoutY(40); 
        
        Rectangle rect = new Rectangle();
        rect.setWidth(900); 
        rect.setHeight(500); 
        rect.setFill(Color.web("#f5f5f5"));
        //Some OuterGlow Effect
        rect.setEffect(DropShadowBuilder.create().                   
                color(Color.web("#969696")).
                offsetX(0).offsetY(0).radius(50).spread(0.2)
                .build());
        
        group.getChildren().add(rect); 
        
        return group;
    }

    
    
    /**
     * Main start function for configuring the GUI Components
     * @param stage
     * @throws Exception 
     */
    @Override
    public void start(Stage stage) throws Exception {      
        
        //Main Group of the Application
        Group group = getBackground();
        //VBox for the Text and Table
        VBox box = new VBox();
        box.setLayoutX(20);
        box.setLayoutY(5);
        box.setSpacing(15); 
        
        //Text
        Text text = new Text("Arvosanat");
        text.setFont(new Font(20)); 
        
        //Table
        TableView table = getTableView();
        table.setLayoutX(20); table.setLayoutY(20); 
        
        //adding all components
        box.getChildren().addAll(text,table);                
        group.getChildren().add(box); 
        
        
        
        Scene scene = new Scene(group,1000,600,Color.web("#666666"));
        stage.setScene(scene);
        stage.show();
    }
    
    public static void main(String[] args) {
        launch(args);
    }
    
}
