package toiwo;


// Luokassa tarvittavat importit
import java.awt.event.MouseEvent;
import java.io.InputStream;
import javafx.scene.control.Label;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import javafx.animation.Animation;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.geometry.Rectangle2D;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.ComboBoxListCell;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.stage.Screen;
import javafx.stage.Stage;
import javafx.util.Duration;


public class Main extends Application {
        // Luodaan ensimmäiseen näkymään buttonit, joilla voidaan avata haluttu näkymä
	private Button lisaaO;
	private Button lisaaK;
        private Button naytaO;
        private Button naytaK;
        
        
        public void naytaOppilaatKurssilla(Kurssi kurssi){
            ListView<String> list = new ListView<String>();
            ArrayList<Oppilas> lista = controller.haeKaikkiOppilaatKurssilta(kurssi.getKurssin_id());  //Haetaan oppilaiden tiedot tietokannasta ja lisätään ne ArrayListiin
            ObservableList<String> oppilaat =FXCollections.observableArrayList();
            for(int i = 0; i < lista.size(); i++){
                String tallenne = "Id: " + lista.get(i).getId() + " Etunimi: " + lista.get(i).getEtunimi() + " Sukunimi: " + lista.get(i).getSukunimi();
                oppilaat.add(tallenne);
            }
            list.setItems(oppilaat);
            list.setPrefWidth(400);
            list.setPrefHeight(800);
            
            final ListView listView = new ListView(oppilaat);
            listView.setPrefSize(500, 900);            
            listView.setCellFactory(ComboBoxListCell.forListView(oppilaat)); 
            
                        
            // Luodaan button jota painamalla voidaan poistaa valittu olemassa oleva oppilas
            final Button removeButton = new Button("Poista valittu oppilas kurssilta");
            removeButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override public void handle(ActionEvent event) {
            final int selectedIdx = listView.getSelectionModel().getSelectedIndex();
            if (selectedIdx != -1) {
 
            final int newSelectedIdx =
            (selectedIdx == listView.getItems().size() - 1)
               ? selectedIdx - 1
               : selectedIdx;
 
            listView.getItems().remove(selectedIdx);
            listView.getSelectionModel().select(newSelectedIdx);
        }
            controller.näytäPopup(controller.poistaOppilas(lista.get(selectedIdx).getId()), "poistaOppilas");   //Ilmoitetaan käyttäjälle onnistuiko poisto vai ei
      }
    });
              
            //Luodaan HBox, johon sijoitetaan poisto Button
            final Label leipeli = new Label();
            leipeli.setText(kurssi.getKurssinNimi());
            
            final HBox controls = new HBox(20);
            controls.setAlignment(Pos.BOTTOM_CENTER);
            controls.getChildren().addAll(removeButton);
    
            StackPane root = new StackPane();
            root.getChildren().setAll(
                listView, 
                controls,
                leipeli
                 );
            primaryStage = getPrimary();
            Scene scene = getScene();
            grid.add(root, 2, 1);
            primaryStage.setScene(scene);
            
        }
        
        
        public void lisaaOppilasKurssille(int kurssiID){
            ListView<String> list = new ListView<String>();
            ArrayList<Oppilas> lista = controller.haeKaikkiOppilaat();  //Haetaan oppilaiden tiedot tietokannasta ja lisätään ne ArrayListiin
            ObservableList<String> oppilaat =FXCollections.observableArrayList();
            for(int i = 0; i < lista.size(); i++){
                String tallenne = "Id: " + lista.get(i).getId() + " Etunimi: " + lista.get(i).getEtunimi() + " Sukunimi: " + lista.get(i).getSukunimi();
                oppilaat.add(tallenne);
            }
            list.setItems(oppilaat);
            list.setPrefWidth(400);
            list.setPrefHeight(800);
            
            final ListView listView = new ListView(oppilaat);
            listView.setPrefSize(500, 900);            
            listView.setCellFactory(ComboBoxListCell.forListView(oppilaat)); 
            
                        
            // Luodaan button jota painamalla voidaan poistaa valittu olemassa oleva oppilas
            final Button removeButton = new Button("Poista valittu oppilas kurssilta");
            removeButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override public void handle(ActionEvent event) {
            final int selectedIdx = listView.getSelectionModel().getSelectedIndex();
            if (selectedIdx != -1) {
 
            final int newSelectedIdx =
            (selectedIdx == listView.getItems().size() - 1)
               ? selectedIdx - 1
               : selectedIdx;
 
            listView.getItems().remove(selectedIdx);
            listView.getSelectionModel().select(newSelectedIdx);
        }
            controller.näytäPopup(controller.poistaOppilas(lista.get(selectedIdx).getId()), "poistaOppilas");   //Ilmoitetaan käyttäjälle onnistuiko poisto vai ei
      }
    });
            
            final Button lisaaO = new Button("Lisää valittu oppilas kurssille");
            lisaaO.setOnAction(new EventHandler<ActionEvent>() {
            @Override public void handle(ActionEvent event) {
            final int selectedIdx = listView.getSelectionModel().getSelectedIndex();
            if (selectedIdx != -1) {
 
            final int newSelectedIdx =
            (selectedIdx == listView.getItems().size() - 1)
               ? selectedIdx - 1
               : selectedIdx;
 
            listView.getItems();
            listView.getSelectionModel().select(newSelectedIdx);
            controller.näytäPopup(controller.cretaOppilasKurssi(lista.get(selectedIdx).getId(), kurssiID), "lisääOppilasKurssi");
            
        }
      }
    });
            
            
            
            //Luodaan HBox, johon sijoitetaan poisto Button
            final HBox controls = new HBox(20);
            controls.setAlignment(Pos.BOTTOM_CENTER);
            controls.getChildren().addAll(removeButton,lisaaO);
    
            StackPane root = new StackPane();
            root.getChildren().setAll(
                listView, 
                controls
                 );
            Scene scene = getScene();
            grid = getGrid();
            grid.add(root ,2 ,1);
            primaryStage = getPrimary();
            primaryStage.setScene(scene);

            
        }
            
        
        
        //Metodi joka luo käyttäjälle näkymän, josta pääsee muokkaamaan valitun opiskelijan tietoja
	public void muokkaaOppilas(String etu, String suku, int id){
            GridPane grid = new GridPane();
            grid.setPadding(new Insets(10, 10, 10, 10));
            grid.setVgap(5);
            grid.setHgap(5);
            
            //Tekstikenttä johon käyttäjä voi syöttää uuden opiskelijan etunimen
            final TextField etunimi = new TextField();
            etunimi.setText(etu);
            etunimi.setPrefColumnCount(12);
            etunimi.getText();
            GridPane.setConstraints(etunimi, 0, 0);
            grid.getChildren().add(etunimi);
            
            //Tekstikenttä johon käyttäjä voi syöttää uuden opiskelijan sukunimen
            final TextField sukunimi = new TextField();
            sukunimi.setText(suku);
            GridPane.setConstraints(sukunimi, 0, 1);
            grid.getChildren().add(sukunimi);
            
            //Tekstikenttä johon käyttäjä voi syöttää uuden opiskelijan ID:n
            final TextField Opnum = new TextField();
            Opnum.setPrefColumnCount(15);
            Opnum.setText(Integer.toString(id));
            GridPane.setConstraints(Opnum, 0, 2);
            grid.getChildren().add(Opnum);
            
            // Luodaan button jota painamalla lähetetään uuden oppilaan luonti tietokantaan
            Button lähetä = new Button("Lähetä");
            GridPane.setConstraints(lähetä, 1, 0);
            grid.getChildren().add(lähetä);

            lähetä.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
            controller.näytäPopup(controller.updateOppilas(Integer.parseInt(Opnum.getText()), etunimi.getText(), sukunimi.getText(), 0), "tallennaOppilas");
            naytaOppilaat();
            
            
            }
    });
            //Luodaan button jota painamalla käyttäjä voi tyhjentää tekstikentät
            Button pyyhi = new Button("Pyyhi");
            GridPane.setConstraints(pyyhi, 1, 1);
            grid.getChildren().add(pyyhi);

            pyyhi.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
            etunimi.clear();
            sukunimi.clear();
            Opnum.clear();
         }
    });

            StackPane root = new StackPane();
            root.getChildren().add(grid);

            Scene scene = getScene();
            primaryStage = getPrimary();
            grid = getGrid();
            grid.add(root,2, 1);
            primaryStage.setScene(scene);

    }
        
        
    //Metodi joka luo käyttäjälle näkymän, josta pääsee muokkaamaan valitun opiskelijan tietoja
	public void muokkaaKurssi(String nimi, String kuva, int kID){
            GridPane grid = new GridPane();
            grid.setPadding(new Insets(10, 10, 10, 10));
            grid.setVgap(5);
            grid.setHgap(5);
            
            //Luodaan uusi tekstikenttä johon käyttäjä voi syöttää uuden kurssin nimen
            final TextField kurssinnimi = new TextField();
            kurssinnimi.setText(nimi);
            kurssinnimi.setPrefColumnCount(12);
            kurssinnimi.getText();
            GridPane.setConstraints(kurssinnimi, 0, 0);
            grid.getChildren().add(kurssinnimi);
            
            //Luodaan uusi tekstikenttä johon käyttäjä voi syöttää uuden kurssin kuvauksen
            final TextField kuvaus = new TextField();
            kuvaus.setPrefHeight(400);

            kuvaus.setText(kuva);
            GridPane.setConstraints(kuvaus, 0, 1);
            grid.getChildren().add(kuvaus);
            
            //Luodaan uusi tekstikenttä johon käyttäjä voi syöttää uuden kurssin ID:n
            final TextField kid = new TextField();
            kid.setPrefColumnCount(15);
            kid.setText(Integer.toString(kID));
            GridPane.setConstraints(kid, 0, 2);
            grid.getChildren().add(kid);
            
            //Luodaan uusi button jota painamalla käyttäjä voi lähettää uuden kurssin tiedot tietokantaan
            Button lähetä = new Button("Lähetä");
            GridPane.setConstraints(lähetä, 1, 0);
            grid.getChildren().add(lähetä);

            lähetä.setOnAction(new EventHandler<ActionEvent>() {    
            @Override
            public void handle(ActionEvent event) {
            controller.näytäPopup(controller.updateKurssi(Integer.parseInt(kid.getText()),kurssinnimi.getText(), kuvaus.getText()), "tallennaKurssi");
        }
    });
            //Luodaan button jota painamalla käyttäjä voi tyhjentää tekstikenttien tiedot
            Button pyyhi = new Button("Pyyhi");
            GridPane.setConstraints(pyyhi, 1, 1);
            grid.getChildren().add(pyyhi);

            pyyhi.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
            kurssinnimi.clear();
            kid.clear();
             kuvaus.clear();
         }
    });

            StackPane root = new StackPane();
            root.getChildren().add(grid);

            Scene scene = getScene();
            primaryStage = getPrimary();
            grid = getGrid();
            grid.add(root, 2, 1);
            primaryStage.setScene(scene);

	}
        
        
        
        
        
        
        
        
        
        
        
        
        //Metodi joka luo käyttäjälle näkymän, josta pääsee luomaan uuden opiskelijan
	public void lisaaOpiskelija(){
            GridPane grid = new GridPane();
            grid.setPadding(new Insets(10, 10, 10, 10));
            grid.setVgap(5);
            grid.setHgap(5);
            
            //Tekstikenttä johon käyttäjä voi syöttää uuden opiskelijan etunimen
            final TextField etunimi = new TextField();
            etunimi.setPromptText("Anna opiskelijan etunimi");
            etunimi.setPrefColumnCount(12);
            etunimi.getText();
            GridPane.setConstraints(etunimi, 0, 0);
            grid.getChildren().add(etunimi);
            
            //Tekstikenttä johon käyttäjä voi syöttää uuden opiskelijan sukunimen
            final TextField sukunimi = new TextField();
            sukunimi.setPromptText("Anna opiskelijan sukunimi");
            GridPane.setConstraints(sukunimi, 0, 1);
            grid.getChildren().add(sukunimi);
            
            //Tekstikenttä johon käyttäjä voi syöttää uuden opiskelijan ID:n
            final TextField Opnum = new TextField();
            Opnum.setPrefColumnCount(15);
            Opnum.setPromptText("Anna opiskelijanumero (ID)");
            GridPane.setConstraints(Opnum, 0, 2);
            grid.getChildren().add(Opnum);
            
            // Luodaan button jota painamalla lähetetään uuden oppilaan luonti tietokantaan
            Button lähetä = new Button("Lähetä");
            GridPane.setConstraints(lähetä, 1, 0);
            grid.getChildren().add(lähetä);

            lähetä.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
            controller.näytäPopup(controller.createOppilas(Integer.parseInt(Opnum.getText()), etunimi.getText(), sukunimi.getText(), 0), "tallennaOppilas");
            }
    });
            //Luodaan button jota painamalla käyttäjä voi tyhjentää tekstikentät
            Button pyyhi = new Button("Pyyhi");
            GridPane.setConstraints(pyyhi, 1, 1);
            grid.getChildren().add(pyyhi);

            pyyhi.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
            etunimi.clear();
            sukunimi.clear();
            Opnum.clear();
         }
    });

            StackPane root = new StackPane();
            root.getChildren().add(grid);

            Scene scene = getScene();
            primaryStage = getPrimary();
            grid = getGrid();
            grid.add(root, 2, 1);
            primaryStage.setScene(scene);

    }
        //Metodi jolla luodaan uusi näkymä käyttäjälle, jossa voidaan luoda uusia kursseja
        public void lisaaKurssi(){
            GridPane grid = new GridPane();
            grid.setPadding(new Insets(10, 10, 10, 10));
            grid.setVgap(5);
            grid.setHgap(5);
            
            //Luodaan uusi tekstikenttä johon käyttäjä voi syöttää uuden kurssin nimen
            final TextField kurssinnimi = new TextField();
            kurssinnimi.setPromptText("Anna kurssin nimi");
            kurssinnimi.setPrefColumnCount(12);
            kurssinnimi.getText();
            GridPane.setConstraints(kurssinnimi, 0, 0);
            grid.getChildren().add(kurssinnimi);
            
            //Luodaan uusi tekstikenttä johon käyttäjä voi syöttää uuden kurssin kuvauksen
            final TextField kuvaus = new TextField();
            kuvaus.setPrefHeight(400);

            kuvaus.setPromptText("Anna kurssin kuvaus");
            GridPane.setConstraints(kuvaus, 0, 1);
            grid.getChildren().add(kuvaus);
            
            //Luodaan uusi tekstikenttä johon käyttäjä voi syöttää uuden kurssin ID:n
            final TextField kid = new TextField();
            kid.setPrefColumnCount(15);
            kid.setPromptText("Anna kurssin id");
            GridPane.setConstraints(kid, 0, 2);
            grid.getChildren().add(kid);
            
            //Luodaan uusi button jota painamalla käyttäjä voi lähettää uuden kurssin tiedot tietokantaan
            Button lähetä = new Button("Lähetä");
            GridPane.setConstraints(lähetä, 1, 0);
            grid.getChildren().add(lähetä);

            lähetä.setOnAction(new EventHandler<ActionEvent>() {    
            @Override
            public void handle(ActionEvent event) {
            controller.näytäPopup(controller.createKurssi(kurssinnimi.getText(), Integer.parseInt(kid.getText()), kuvaus.getText()), "tallennaKurssi");
        }
    });
            //Luodaan button jota painamalla käyttäjä voi tyhjentää tekstikenttien tiedot
            Button pyyhi = new Button("Pyyhi");
            GridPane.setConstraints(pyyhi, 1, 1);
            grid.getChildren().add(pyyhi);

            pyyhi.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
            kurssinnimi.clear();
            kid.clear();
             kuvaus.clear();
         }
    });

            StackPane root = new StackPane();
            root.getChildren().add(grid);

            Scene scene = getScene();
            primaryStage = getPrimary();
            grid = getGrid();
            grid.add(root, 2, 1);
            primaryStage.setScene(scene);

	}
        
        //Metodi joka luo käyttäjälle näkymän, jossa listataan kaikki olemassa olevat oppilaat ja heidän tiedot
        public void naytaOppilaat(){
            ListView<String> list = new ListView<String>();
            ArrayList<Oppilas> lista = controller.haeKaikkiOppilaat();  //Haetaan oppilaiden tiedot tietokannasta ja lisätään ne ArrayListiin
            ObservableList<String> oppilaat =FXCollections.observableArrayList();
            for(int i = 0; i < lista.size(); i++){
                String tallenne = "Id: " + lista.get(i).getId() + " Etunimi: " + lista.get(i).getEtunimi() + " Sukunimi: " + lista.get(i).getSukunimi();
                oppilaat.add(tallenne);
            }
            list.setItems(oppilaat);
            list.setPrefWidth(400);
            list.setPrefHeight(800);
            
            final ListView listView = new ListView(oppilaat);
            listView.setPrefSize(500, 900);            
            listView.setCellFactory(ComboBoxListCell.forListView(oppilaat)); 
            
                        
            // Luodaan button jota painamalla voidaan poistaa valittu olemassa oleva oppilas
            final Button removeButton = new Button("Poista valittu oppilas");
            removeButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override public void handle(ActionEvent event) {
            final int selectedIdx = listView.getSelectionModel().getSelectedIndex();
            if (selectedIdx != -1) {
 
            final int newSelectedIdx =
            (selectedIdx == listView.getItems().size() - 1)
               ? selectedIdx - 1
               : selectedIdx;
 
            listView.getItems().remove(selectedIdx);
            listView.getSelectionModel().select(newSelectedIdx);
        }
            controller.näytäPopup(controller.poistaOppilas(lista.get(selectedIdx).getId()), "poistaOppilas");   //Ilmoitetaan käyttäjälle onnistuiko poisto vai ei
      }
    });
            
            final Button editO = new Button("Muokkaa valitun oppilaan tietoja");
            editO.setOnAction(new EventHandler<ActionEvent>() {
            @Override public void handle(ActionEvent event) {
            final int selectedIdx = listView.getSelectionModel().getSelectedIndex();
            if (selectedIdx != -1) {
 
            final int newSelectedIdx =
            (selectedIdx == listView.getItems().size() - 1)
               ? selectedIdx - 1
               : selectedIdx;
 
            listView.getItems();
            listView.getSelectionModel().select(newSelectedIdx);
            muokkaaOppilas(lista.get(selectedIdx).getEtunimi(),lista.get(selectedIdx).getSukunimi(),lista.get(selectedIdx).getId());
            
        }
      }
    });
            
            
            
            //Luodaan HBox, johon sijoitetaan poisto Button
            final HBox controls = new HBox(20);
            controls.setAlignment(Pos.BOTTOM_CENTER);
            controls.getChildren().addAll(removeButton,editO);
    
            StackPane root = new StackPane();
            root.getChildren().setAll(
                listView, 
                controls
                 );
            primaryStage = getPrimary();
            Scene scene = getScene();
            grid.add(root, 2, 1);
            primaryStage.setScene(scene);
            
        }
        
        
        private Kurssi kurssi;
        //Metodi joka luo käyttäjälle näkymän jossa esillä on olemassa olevat kurssit
        public void naytaKurssit(){
            ListView<String> list = new ListView<String>();
            ArrayList<Kurssi> lista = controller.haeKaikkiKurssit();
            ObservableList<String> kurssit =FXCollections.observableArrayList();
            for(int i = 0; i < lista.size(); i++){
                String tallenne = "Id: " + lista.get(i).getKurssin_id() + " Kurssin nimi: " + lista.get(i).getKurssinNimi();
                kurssit.add(tallenne);
            }
            list.setItems(kurssit);
            list.setPrefWidth(400);
            list.setPrefHeight(800);
            
            final ListView listView = new ListView(kurssit);
            listView.setPrefSize(500, 900);
            listView.setEditable(true);
            
            listView.setCellFactory(ComboBoxListCell.forListView(kurssit));  
        // Luodaan button jota painamalla voidaan poistaa valittu olemassa oleva kurssi   
        final Button removeButton = new Button("Poista valittu kurssi");
            removeButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override public void handle(ActionEvent event) {
            final int selectedIdx = listView.getSelectionModel().getSelectedIndex();
            if (selectedIdx != -1) {
 
            final int newSelectedIdx =
            (selectedIdx == listView.getItems().size() - 1)
               ? selectedIdx - 1
               : selectedIdx;
 
            listView.getItems().remove(selectedIdx);
            listView.getSelectionModel().select(newSelectedIdx);
        }
            controller.näytäPopup(controller.poistaKurssi(lista.get(selectedIdx).getKurssin_id()), "poistaKurssi"); //Ilmoitetaan käyttäjälle onnistuiko toimenpide
      }
    });
            final Button editK = new Button("Muokkaa valitun kurssin tietoja");
            editK.setOnAction(new EventHandler<ActionEvent>() {
            @Override public void handle(ActionEvent event) {
            final int selectedIdx = listView.getSelectionModel().getSelectedIndex();
            if (selectedIdx != -1) {
 
            final int newSelectedIdx =
            (selectedIdx == listView.getItems().size() - 1)
               ? selectedIdx - 1
               : selectedIdx;
 
            listView.getItems();
            listView.getSelectionModel().select(newSelectedIdx);
            muokkaaKurssi(lista.get(selectedIdx).getKurssinNimi(),lista.get(selectedIdx).getKurssinKuvaus(),lista.get(selectedIdx).getKurssin_id());
            
        }
      }
    });
            final Button lisaaK = new Button("Lisää oppilaita kurssille");
            lisaaK.setOnAction(new EventHandler<ActionEvent>(){
                @Override public void handle(ActionEvent event) {
                    final int selectedIdx = listView.getSelectionModel().getSelectedIndex();
                    lisaaOppilasKurssille(lista.get(selectedIdx).getKurssin_id());
                    
                }
            });
            
            final Button naytaO = new Button("Näytä osallistujat");
            naytaO.setOnAction(new EventHandler<ActionEvent>(){
                @Override public  void handle(ActionEvent event) {
                    final int selectedIdx = listView.getSelectionModel().getSelectedIndex();
                    naytaOppilaatKurssilla(lista.get(selectedIdx));
                }
            });
            
            //Luodaan HBox, johon poisto button sijoitetaan
            final HBox controls = new HBox(10);
            controls.setAlignment(Pos.BOTTOM_CENTER);
            controls.getChildren().addAll(removeButton,editK,lisaaK,naytaO);
            
            grid = getGrid();
            Scene scene = getScene();
            primaryStage = getPrimary();
            
            StackPane root = new StackPane();
            root.getChildren().setAll(
                listView, 
                controls
                 );
            grid.add(root, 2, 1);
            primaryStage.setScene(scene);

            
        }
        
        private Stage primaryStage;
        private GridPane grid;
        
        public void setGrid(GridPane grid){
            this.grid = grid;
        }
        
        public GridPane getGrid(){
            return grid;
        }

        public Stage getPrimary(){
            return primaryStage;
        }
        
        public void setPrimary(Stage primaryStage){
            this.primaryStage = primaryStage;
        }


        @Override
	public void start(Stage primaryStage) throws Exception {
            Scene scene = getScene();
            setPrimary(primaryStage);
            primaryStage.setTitle("Toiwo");
            primaryStage.setScene(scene);
            Rectangle2D primaryScreenBounds = Screen.getPrimary().getVisualBounds();
            primaryStage.setX(primaryScreenBounds.getMinX());
            primaryStage.setY(primaryScreenBounds.getMinY());
            primaryStage.setWidth(primaryScreenBounds.getWidth());
            primaryStage.setHeight(primaryScreenBounds.getHeight());
            primaryStage.show();
	}
        
	
public static void main(String[] args) {
	launch(args);
}

private Controller controller;
private DataBaseConnector tietokanta;
private DisplayConfirmation displayPopup;
private clock cock;
private Grafiikka grafiikka;

public void init(){
    grafiikka = new Grafiikka();
    tietokanta = new DataBaseConnector();
    displayPopup = new DisplayConfirmation();
    controller = new Controller(this, tietokanta, displayPopup, grafiikka);
    controller.connectToTietokanta();
    
    
    }

public Scene getScene(){
    lisaaO = new Button();
    lisaaO.setText("Lisää uusi opiskelija");
    lisaaO.setOnAction((ActionEvent event) -> {
        lisaaOpiskelija();
    });

    lisaaK = new Button();
    lisaaK.setText("Lisää uusi kurssi");
    lisaaK.setOnAction((ActionEvent event) -> {
        lisaaKurssi();
    });

    naytaO = new Button();
    naytaO.setText("Näytä lista oppilaista");
    naytaO.setOnAction((ActionEvent event) -> {
        naytaOppilaat();
    });

    naytaK = new Button();
    naytaK.setText("Näytä lista kursseista");
    naytaK.setOnAction((ActionEvent event) -> {
        naytaKurssit();
    });

    GridPane grid = new GridPane();
    grid.setAlignment(Pos.TOP_CENTER);
    grid.setVgap(20);
    grid.setHgap(20);

    grid.add(lisaaO,0,0);
    grid.add(lisaaK,1,0);
    grid.add(naytaO,3,0);
    grid.add(naytaK,4,0);

    final Label cock = new Label();  
    final DateFormat format = DateFormat.getInstance();  
    final Timeline timeline = new Timeline(new KeyFrame(Duration.seconds(1), new EventHandler<ActionEvent>() {  
    @Override  
    public void handle(ActionEvent event) {  
    final Calendar cal = Calendar.getInstance();  
    cock.setText(format.format(cal.getTime()));  
        }  
    }));  
    
    grid.add(cock,5,0);
    setGrid(grid);
    grid.setGridLinesVisible(true);
    

    
    
    Scene scene = new Scene(grid);
    timeline.setCycleCount(Animation.INDEFINITE);  
    timeline.play(); 
    return scene;
}
}
