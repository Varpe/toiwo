/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package toiwo;

/**
 *
 * @author Hannu
 */
public class Arvosana {
    
    private int oppilas_id;
    private int kurssin_id;
    private int arvosana;
    
    public Arvosana(){
        oppilas_id = 0;
        kurssin_id = 0;
        arvosana = 0;
    }
    
    public Arvosana(int oppilas_id, int kurssin_id, int arvosana){
        this.oppilas_id = oppilas_id;
        this.kurssin_id = kurssin_id;
        this.arvosana = arvosana;
    }
    
    public int getOppilas_id(){
        return oppilas_id;
    }
    
    public int getKurssin_id(){
        return kurssin_id;
    }
    
    public int getArvosana(){
        return arvosana;
    }
    
    public void setOppilas_id(int oppilas_id){
        this.oppilas_id = oppilas_id;
    }
    
    public void setKurssin_id(int kurssin_id){
        this.kurssin_id = kurssin_id;
    }
    
    public void setArvosana(int arvosana){
//        this.arvosana = arvosana;
    }
    
}
