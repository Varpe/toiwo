/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package toiwo;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

/**
 *
 * @author Hannu
 */
public class DataBaseConnector { // Luokka jossa on kaikki tietokantaa liittyvät metodit
    
    private Connection connection;
    private Statement statement;
    private ResultSet result;
    private final String DataBaseUrl = "jdbc:mysql://localhost:3306/toiwo"; // tietokannan osoite
    private final String USER = "toiwo"; // käyttäjätunnus
    private final String PASS = "lollero"; // salasana
    private Oppilas oppilas;
    private Kurssi kurssi;

    // Metodi jolla muodostetaan yhteys tietokantaan
    // Palauttaa true jos yhteys onnistui, false jos yhteyttä ei saatu
    public boolean Connect(){
        try {
            Class.forName("com.mysql.jdbc.Driver");
            connection = DriverManager.getConnection(DataBaseUrl, USER, PASS); // Yritetään yhdistää tietokantaan
            statement = connection.createStatement(); // Luodaan statement
            System.out.println("Yhteys tietokantaan muodostettu.");
            return true; // palautetaan tosi jos ei onnistunut
            
        } catch (Exception e) { // virheen sattuessa
            System.out.println("Yhteys tietokantaan ei onnistunut!");
            System.out.println(e);
            return false; // palautetaan false jos ei onnistunut
        }
    }
    
 
    
    // Metodilla luodaan uusi kurssi
    // Kurssi luodaan parametrien perusteella
    // Palautetaan true jos kurssin luominen onnistui, false jos se epäonnistui
    public boolean createKurssi(String kurssinNimi, int kurssinId, String kuvaus){
        try { // tarkistetaaan että tietokannassa ei ole kurssia jolla olisi sama id
            String query = "select kurssin_id from kurssi"; // komento jolla etsitään kaikkien kurssin id:t
            result = statement.executeQuery(query); // suoritetaan komento
            while(result.next()){ // verrataan kurssien id:eitä luotavan kurssin id:seen
                if(result.getInt("kurssin_id") == kurssinId){ // jos tietokannasta löytyy kurssi jolla on jo id käytössä
                    return false; // ei anneta luoda kurssia
                }
            }
        } catch (Exception e) {
            System.out.println("Virhe createOppilas(), uniikin opiskelijanumeron tarkistus!");
            return false; // virheen sattuessa palautetaan false
        }
        
        try { // jos kurssin id ei ollut käytössä yritetään luoda uusi kurssi
            String query = "insert into kurssi values('" + kurssinId + "', '" + kurssinNimi + "', '" + kuvaus + "')"; // komento jolla kurssi luodaan
            statement.executeUpdate(query); // suoritetaan komento
            System.out.println("Luotu kurssi.");
            return true; // palautetaan true jos kurssin luonti onnistui
        } catch (Exception e) { // virheen sattuessa
            System.out.println("Virhe createKurssi()!");
            return false; // palautetaan false jos kurssin luonti ei onnistunut
        }
    }
    
   // Metodi luo uuden oppilaan annetuilla parametreillä (int suoritukset on vakio 0)
   // Palauttaa true jos oppilaan luonti onnitui, false jos se ei onnistunut
    public boolean createOppilas(int oppilas_id, String etunimi, String sukunimi, int suoritukset){
        try { // tarkistetaan että tietokannassa ei ole oppilasta samalla id:llä
            String query = "select oppilas_id from oppilas"; // komento jolla haetaan tietokannassa olevien oppilaiden id:t
            result = statement.executeQuery(query); // komennon suorittaminen
            while(result.next()){ // verrataan annettua id:tä tietokannassa oleviin id:isiin
                if(result.getInt("oppilas_id") == oppilas_id){ // jos löytyi sama id
                    return false; // estetään oppilaan luonti
                }
            }
        } catch (Exception e) {
            System.out.println("Virhe createOppilas(), uniikin opiskelijanumeron tarkistus!");
            return false; // virheen sattuessa palautetaan false
        }
        
        try { // jos tietokannasta ei löytynyt samaa id:tä yritetään luoda oppilas 
            String query = "insert into oppilas values('" + oppilas_id + "', '" + etunimi + "', '" + sukunimi + "', '" + suoritukset + "')"; // komento jolla luodaan oppilas
            statement.executeUpdate(query); // suoritetaan komento
            System.out.println("Luotu oppilas!");
            return true; // palautetaan tosi jos onnistui
        } catch (Exception e) {
            System.out.println("Virhe createOppilas()!");
            System.out.println(e);
            return false; // palautetaan false jos ei onnistunut
        }
        
    }
    
    // Metodi jolla haetaan tietokannasta kaikki oppilaat
    // Palauttaa listan Oppilas-olioita
    public ArrayList<Oppilas> haeKaikkiOppilaat(){
        try {
            String query = "select * from oppilas"; // komento jolla haetaan kaikkien oppilaiden tiedot
            result = statement.executeQuery(query); // suoritetaan komento
            ArrayList<Oppilas> lista = new ArrayList<>(); // luodaan oppilas lista jonne oppilaat sijoitetaan
            
            while(result.next()){ // luodaan oppilaita niin kauan kun niitä löytyy
                // poimitaan tietokannasta oppilaantiedot
                int oppilas_id = result.getInt("oppilas_id");
                String etunimi = result.getString("etunimi");
                String sukunimi = result.getString("sukunimi");
                int suoritukset = result.getInt("suoritukset");
                
                oppilas = new Oppilas(oppilas_id, etunimi, sukunimi, suoritukset); // luodaan uusi oppilas saaduilla tiedoilla
                lista.add(oppilas); // lisätään oppilas listaan
            }
            return lista; // palautetaan oppilas lista
        } catch (Exception e) {
            System.out.println("Virhe haeKaikkiOppilaat()!");
            System.out.println(e);
            return null; // palautetaan null jos ei onnistunut
        }
    }
    
    // Metodi jolla haetaan kaikki kurssit
    // Palauttaa listan Kurssi-olioita
    public ArrayList<Kurssi> haeKaikkiKurssit(){
        try {
            String query = "select * from kurssi"; // komento jolla haetaan kaikkien kurssien tiedot
            result = statement.executeQuery(query); // suoritetaan komento
            ArrayList<Kurssi> lista = new ArrayList<>(); // luodaan lista jonne viedään kurssit
            // alustetaan kurssiin tarvittavat atribuutit
            int kurssin_id;
            String kurssinNimi;
            String kurssinKuvaus;
            
            while(result.next()){ // luodaan kursseja niin kauan kun niitä löytyy tietokannasta
                kurssin_id = result.getInt("kurssin_id");
                kurssinNimi = result.getString("kurssinnimi");
                kurssinKuvaus = result.getString("kuvaus");
                
                kurssi = new Kurssi(kurssin_id, kurssinNimi, kurssinKuvaus); // luodaan uusi kurssi olio saaduilla tiedoilla
                lista.add(kurssi); // lisätään kurssi listaan
            }
            return lista; // palautetaan lista
        } catch (Exception e) {
            System.out.println("Virhe haeKaikkiKurssit()!");
            System.out.println(e);
            return null; // palautetaan null jos ei onnistunut
        }
    }
    
    // Metodilla voidaan poistaa oppilas
    // Poistettava oppilas tunnistetaan parametrillä oppilas_id
    // Palautetaan true jos poisto onnistui, false jos poistaminen epäonnistui
    public boolean poistaOppilas(int oppilas_id){
        try {
            String query = "delete from oppilas where oppilas_id=" + oppilas_id; // komento poistetaan oppilas, oppilas etsitään uniikilla id:llä
            statement.executeUpdate(query); // suoritetaan komento
            System.out.println("Poistettu oppilas id:llä " + oppilas_id);
            return true; // palautetaan true jos onnistui
        } catch (Exception e) {
            System.out.println("Virhe poistaOppilas!");
            System.out.println(e);
            return false; // palautetaan false jos ei onnistunut
        }
    }
    
    // Metodi jolla voidaan poistaa kurssi
    // Poistettava kurssin tunnistetaan parametrina saadulla kurssin_id:llä
    // Palautetaan true jos kurssin poistaminen onnistui, false jos se ei onnistunut
    public boolean poistaKurssi(int kurssin_id){
        try {
            String query = "delete from kurssi where kurssin_id=" + kurssin_id; // komento jolla poistetaan kurssi, kurssi etsitään uniikilla id:llä
            statement.executeUpdate(query); // suoritetaan komento
            System.out.println("Poistettu kurssi id:llä " + kurssin_id);
            return true; // palautetaan true jos onnistui
        } catch (Exception e) {
            System.out.println("Virhe poistaKurssi!");
            System.out.println(e);
            return false; // palautetaan false jos ei onnistunut
        }
    }
    

    // Metodi jolla päivitetään oppilas, saa parametriksi oppilaan tiedot, joista jokin on voinut muuttua
    // Päivitetään kuitenkin kaikki tiedot, jos tiedot eivät ole päivittyneet laitetaan vanhat tiedot
    // Päivitettä oppilas tunnistetaan parametrinä saadulla oppilas_id:llä
    // Palautetaan true jos oppilaan päivittäminen onnistui, false jos päivittäminen ei onnistunut
    public boolean updateOppilas(int oppilas_id, String etunimi, String sukunimi, int suoritukset){
        try {
            String query = "update oppilas set oppilas_id=" + oppilas_id + ", etunimi='" + etunimi + "', sukunimi='" + sukunimi + "', suoritukset=" + suoritukset + " where oppilas_id=" + oppilas_id + ""; // komento jolla oppilas päivitetään
            statement.executeUpdate(query); // suoritetaan komento
            System.out.println("Oppilas päivitetty.");
            return true; // palautetaan true jos onnistui
        } catch (Exception e) {
            System.out.println("Virhe updateOppilas()!");
            System.out.println(e);
            return false; // palautetaan false jos ei onnistunut
        }
    }
    

    // Metodi jolla päivitetään kurssi, saa parametriksi kurssin tiedot, joista jokin on voinut muuttua
    // Päivitetään kuitenkin kaikki tiedot, jos tiedot eivät ole päivittyneet laitetaan vanhat tiedot
    // Päivitettävä kurssi tunnistetaan parametrinä saadulla kurssin_id:llä
    // Palautetaan true jos kurssin päivittäminen onnistui, false jos päivittäminen epäonnistui
    public boolean updateKurssi(int kurssin_id, String kurssinNimi, String kurssinKuvaus){
        try {
            String query = "update kurssi set kurssin_id=" + kurssin_id + ", kurssinnimi='" + kurssinNimi + "', kuvaus='" + kurssinKuvaus + "' where kurssin_id=" + kurssin_id + ""; // komento jolla kurssi päivitetään
            statement.executeUpdate(query); // suoritetaan komento
            System.out.println("Kurssi päivitetty.");
            return true; // palautetaan true jos onnistui
        } catch (Exception e) {
            System.out.println("Virhe updateKurssi()!");
            System.out.println(e);
            return false; // palautetaan false jos ei onnistunut
        }
    }
    
    // Metodille sijoitetaan oppilas kurssille
    // Oppilas ja kurssi tunnistetaan parametrinä saaduilla id:illä
    // Palautetaan true jos oppilas lisättiin kurssille, false jos lisääminen ei onnistunut
    public boolean creataOppilasKurssi(int oppilas_id, int kurssin_id){
        try { // ensin tarkistetaan että oppilas ei ole jo kurssilla ettei synny dublikaatteja
            String query = "select oppilas_id, kurssin_id from oppilas_kurssi where oppilas_id=" + oppilas_id; // jolla haetaan oppilaan kaikki kurssit
            result = statement.executeQuery(query); // suoritetaan haku
            while(result.next()){ // verrataan tuloksia niin kauan kuin niitä on
                // kerätään oppilaan kurssit joilla hän on
                int op_id = result.getInt("oppilas_id");
                int kurs_id = result.getInt("kurssin_id");
                if(op_id == oppilas_id & kurs_id == kurssin_id){ // jos oppilas on jo kurssilla johon hänet yritetään laittaa
                    System.out.println("Oppilas on jo kurssilla");
                    return false; // estetetään oppilaan lisääminen kurssille ja palautetaan false
                }
            }
        } catch (Exception e) {
            System.out.println("Virhe oppilas_kurssi tarkistuksessa!");
            System.out.println(e);
            return false; // virheen sattuessa palautetaan false
        }

        try { // kun oppilas on tarkistettu voidaan hänet lisätä kurssille
            String query = "insert into oppilas_kurssi values(" + oppilas_id + ", " + kurssin_id +")"; // komento jolla oppilas lisätään kurssille
            statement.executeUpdate(query); // suoritetaan komento
            System.out.println("OppilasKurssi luotu!");
            return true; // palautetaan true jos onnistui
        } catch (Exception e) { // virheen sattuessa
            System.out.println("Vihe createOppilasKurssi!");
            System.out.println(e);
            return false; // palautetaan false jos ei onnistunut
        }
    }
    
    private OppilasKurssi oppilas_kurssi;
    
    //Metodi hakee kaikki oppilaat kaikilta kursseilta
    // Palauttaa listan OppilasKurssi-olioita
    public ArrayList<OppilasKurssi> HaeKaikkiOppilasKurssi(){
        try {
            ArrayList<OppilasKurssi> oppilasKurssiLista = new ArrayList(); // luodaan OppilasKurssi-lista jonne oppilasKurssi-oliot sijoitetaan
            String query = "select * from oppilas_kurssi"; // komento jolla haetaan kaikki oppilaat kaikilta kursseilta
            result = statement.executeQuery(query); // suoritetaan komento
            while(result.next()){ // kerätään tiedot talteen niin kauan kuin niitä on jäljellä
                // poimitaan tiedot talteen
                int oppilas_id = result.getInt("oppilas_id");
                int kurssin_id = result.getInt("kurssin_id");
                oppilas_kurssi = new OppilasKurssi(oppilas_id, kurssin_id); // luodaan uusi OppilasKurssi olio
                oppilasKurssiLista.add(oppilas_kurssi); // sijoitetaan luotu olio oppilasKurssi listaan
            }
            return oppilasKurssiLista; // kun ollaan valmiita palautetaan löytyneet OppilasKussit
        } catch (Exception e) {
            System.out.println("Virhe HaeKaikkiOppilasKurssi!");
            System.out.println(e);
            return null; // virheen sattuessa palautetaan null
        }
    }
    
    //Metodi hakee kaikki oppilaat jotka ovat kurssilla jonka kurssin_id saadaan parametrinä
    // Palautetaan OppilasKurssi olio lista
    public ArrayList<OppilasKurssi> HaeKaikkiOppilaatKurssilta(int kurssin_id){
        try {
            ArrayList<OppilasKurssi> oppilaatKurssilla = new ArrayList(); // luodaan lista jonne löydetyt oppilaat kurssilta sijoitetaan
            String query = "select * from oppilas_kurssi where kurssin_id=" + kurssin_id; // komento jolla haetaan oppilaat kurssilta
            result = statement.executeQuery(query); // suoritetaan komento
            while(result.next()){ // poimitaan tiedot talteen niin kauan kuin niitä löytyy
                int oppilas_id = result.getInt("oppilas_id");
                int kurssi_id = result.getInt("kurssin_id");
                oppilas_kurssi = new OppilasKurssi(oppilas_id, kurssi_id); // luodaan saaduilla tiedoilla uusi OppilasKurssi olio
                oppilaatKurssilla.add(oppilas_kurssi); // lisätään luotu olio OppilasKurssi listaan
            }
            return oppilaatKurssilla; // kun ollaan valmiita palautetaan löytyneet oppilaat kurssilta
        } catch (Exception e) {
            System.out.println("Virhe HaeKaikkiOppilaatKurssilta!");
            System.out.println(e);
            return null; // jos sattui virhe palautetaan null
        }
    }
    
    //Metodi hakee kaikki kurssit joilla oppilas on 
    // Oppilas tunnistetaan parametrinä saadulla oppilas_id:llä
    // Palautetaan 
    public ArrayList<OppilasKurssi> HaeKaikkiOppilaanKurssit(int oppilas_id){
        try {
            ArrayList<OppilasKurssi> oppilaanKurssit = new ArrayList(); // luodaan OppilasKurssi lista jonne sijoitetaan kaikki oppilaan kurssit
            String query = "select * from oppilas_kurssi where oppilas_id=" + oppilas_id; // komento jolla haetaan oppilaan kaikki kurssit
            result = statement.executeQuery(query); // suoritetaan haku komento
            while(result.next()){ // poimitaan tietoja talteen niin kauan kuin niitä löytyy
                int oppilaan_id = result.getInt("oppilas_id");
                int kurssi_id = result.getInt("kurssin_id");
                oppilas_kurssi = new OppilasKurssi(oppilaan_id, kurssi_id); // luodaan saaduilla tiedoilla uusi OppilasKurssi olio
                oppilaanKurssit.add(oppilas_kurssi); // lisätään luotu kurssi OppilasKurssi listaan
            }
            return oppilaanKurssit; // kun ollaan valmiita palautetaan saadut kurssit
        } catch (Exception e) {
            System.out.println("Virhe HaeKaikkiOppilaanKurssit!");
            System.out.println(e);
            return null; // virheen sattuessa palautetaan null
        }
    }
    
    // Metodi hakee oppilaat jotka ovat tietyllä kurssilla ja muuttaa ne Oppilas olioiksi
    // Ottaa parametriksi HaeKaikkiOppilaatKurssilta saatavan OppilasKurssiLIstan
    public ArrayList<Oppilas> OppilasKurssiOppilaaksi(ArrayList<OppilasKurssi> oppilasKurssiLista){
        try {
            ArrayList<Oppilas> oppilasLista = new ArrayList(); // luodaan Oppilas olio lista johon saadut oppilaat tallentaan
            for(int i = 0; i < oppilasKurssiLista.size(); i ++){ // haetaan jokainen oppilas ja luodaan oppilas oliot
                String query = "select * from oppilas where oppilas_id=" + oppilasKurssiLista.get(i).getOppilas_id(); // komento jolla haetaan oppilaat
                result = statement.executeQuery(query); // suoritetaan komento
                while(result.next()){ // niin kauan kuin oppilaita on jäljellä
                    // poimitaan tiedot talteen
                    int oppilaan_id = result.getInt("oppilas_id");
                    String etunimi = result.getString("etunimi");
                    String sukunimi = result.getString("sukunimi");
                    int suoritukset = result.getInt("suoritukset");
                    oppilas = new Oppilas(oppilaan_id, etunimi, sukunimi, suoritukset); // luodaan uusi oppilas olio saaduilla tiedoilla
                    oppilasLista.add(oppilas); // lisätään luoto Oppilas Oppilas-listaan
                }
            }
            return oppilasLista; // kun ollaan valmiita palautetaan Oppilas-olio lista
        } catch (Exception e) {
            System.out.println("Virhe OppilasKurssiOppilaaksi!");
            System.out.println(e);
            return null; // virheen sattuessa palautetaan null
        }
    }
    
    // Metodi hakee oppilaan kaikki kurssit ja muuttaa ne kurssi olioiksi
    // Saa parametrinä OppilasKurssi listan joka saadaan HaeKaikkiOppilaanKurssit
    public ArrayList<Kurssi> OppilasKurssiKurssiksi(ArrayList<OppilasKurssi> oppilasKurssiLista){
        try {
            ArrayList<Kurssi> kurssiLista = new ArrayList(); // luodaan lista jonne oppilaan kurssit sijoitetaan
            for(int i = 0; i < oppilasKurssiLista.size(); i++){ // haetaan jokaisen kurssin tiedot
                String query = "select * from kurssi where kurssin_id=" + oppilasKurssiLista.get(i).getKurssin_id(); // hakukomento jolla kurssin tiedot saadaan
                result = statement.executeQuery(query); // suoritetaan hakukomento
                while(result.next()){ // niin kauan kun on hakutuloksia
                    // poimitaan tiedot talteen
                    int kurssinid = result.getInt("kurssin_id");
                    String kurssinNimi = result.getString("kurssinimi");
                    String kurssinKuvaus = result.getString("kuvaus");
                    kurssi = new Kurssi(kurssinid, kurssinNimi, kurssinKuvaus); // luodaan saaduilla tiedoilla uusi kurssi olio
                    kurssiLista.add(kurssi); // lisätään luotu olio kurssilistaan
                }
            }
            return kurssiLista; // kun ollaan valmiita palautetaan lista kursseista
        } catch (Exception e) { // virheen sattuessa
            System.out.println("Virhe OppilasKurssiKurssiksi!");
            System.out.println(e);
            return null; // jos tapahtui virhe palautetaan null
        }
        
    }
    
    // Metodi poistaa tietyn oppilaan tietyltä kurssilta
    // Kurssi ja oppilas tunnistetaan parametreillä oppilas_id ja kurssin_id
    // Palautetaan true jos oppilaan poisto kurssilta onnistui, false jos se epäonnistui
    public boolean PoistaOppilasKurssilta(int oppilas_id, int kurssin_id){
            try {
                String query  = "delete from oppilas_kurssi where oppilas_id=" + oppilas_id + " and kurssin_id=" + kurssin_id; // komento jolla oppilas poistetaan kurssilta
                statement.executeUpdate(query); // suoritetaan komento
                System.out.println("Oppilas poistettu kurssilta.");
                return true; // jos poisto onnistui palautetaan true
            } catch (Exception e) { // virheen sattuessa
                System.out.println("Virhe PoistaOppilasKurssilta!");
                System.out.println(e);
                return false; // palautetaan false
            }
        }
    
    // Metodilla asetataan naamakerroin oppilaalle
    // Oppilas tunnistetaan parametrillä oppilas_id ja hymiö tunnistetaan parametrillä hymiö_id
    public boolean AsetaNaamakerroin(int oppilas_id, int hymiö_id){
        try { // yritetään asettaa naamakerrointa tauluun naamakerroin
            String query = "insert into naamakerroin values(" + oppilas_id +", " + hymiö_id + ")"; // komento jolla naamakerroin asetetaan oppilaalle
            statement.executeUpdate(query); // suoritetaan komento
            return true; // palautetaan tosi jos onnistui
        } catch (Exception e) { // virheen sattuessa
            System.out.println("Virhe AsetaNaamakerroin!");
            System.out.println(e); 
            return false; // palautetaan false jos suorittaminen ei onnistunut
        }
    }
    
   private Naamakerroin naamakerroin;
    
   // Metodi hakee oppilaan naamakertoimen 
   // Oppilas tunnistetaan parametrinä saatavalla oppilas_id:llä
   public ArrayList<Naamakerroin> HaeNaamakerroin(int oppilas_id){
       try {
           String query = "select * from naamakerroin where oppilas_id=" + oppilas_id; // komento jolla haetaan oppilaan naamakerroin
           result = statement.executeQuery(query); // suoritetaan haku
           ArrayList<Naamakerroin> naamakerroinLista = new ArrayList(); // luodaan lista jonne oppilaan naamakerroin olio tallennetaan
           while(result.next()){ // niin kauan kuin on naamakertoimia (oletus että vain yksi per oppilas)
               // otetaan talteen naamakertoimen muuttujat
               int oppilaan_id = result.getInt("oppilas_id");
               int hymiö_id = result.getInt("hymiö_id");
               naamakerroin = new Naamakerroin(oppilaan_id, hymiö_id); // luodaan uusi naamakerroin olio
               naamakerroinLista.add(naamakerroin); // lisätään luotu naamakerroin olio naamakerroin listaan
           }
           return naamakerroinLista; // palautetaan naamakerroin lista
       } catch (Exception e) { // jos ei onnistunut
           System.out.println("Virhe HaeNaamakerroin(int oppilas_id)!");
           System.out.println(e);
           return null; // palautetaan null jos ei onnistunut
       }
   }
   
   
   // Metodi hakee koko kurssin naamakertoimet
   // Oppilaat tunnistetaan parametrinä saatavana oppilasKurssi listasta
   public ArrayList<Naamakerroin> HaeNaamakerroin(ArrayList<OppilasKurssi> oppilasKurssi){
       try {
           ArrayList<Naamakerroin> naamakerroinLista = new ArrayList(); // luodaan naamakerroin olio lista
           for(int i = 0; i < oppilasKurssi.size(); i++){ // suoritetaan naamakertoimen haku jokaisella oppilaalle joka on listalla
               String query = "select * from naamakerroin where oppilas_id=" + oppilasKurssi.get(i).getOppilas_id(); // haku komento jolla haetaan oppilaan naamakerroin
               result = statement.executeQuery(query); // suoritetaan haku
               while(result.next()){ // otetaan talteen naamakertoimen tiedot
                   int oppilaan_id = result.getInt("oppilas_id");
                   int hymiön_id = result.getInt("hymiö_id");
                   naamakerroin = new Naamakerroin(oppilaan_id, hymiön_id); // luodaan uusi naamakerroin olio saaduilla tiedoilla
                   naamakerroinLista.add(naamakerroin); // lisätään olio naamakerroin listaan
               }
           }
           return naamakerroinLista; // palautetaan naamakerroin lista
       } catch (Exception e) { // jos tapahtui virhe
           System.out.println("Virhe HaeNaamakerroin(ArrayList<OppilasKurssi>)!");
           System.out.println(e);
           return null; // palautetaan null jos ei onnistunut
       }
   }
   
   // Metodi hakee kaikkien oppilaiden naamakertoimet ja palauttaa naamakerroin olio listan
  public ArrayList<Naamakerroin> HaeKaikkiNaamakertoimet(){
      try {
          ArrayList<Naamakerroin> naamakerroinLista = new ArrayList(); // luodaan naamakerroin lista jonne naamakerroin oliot tallennetaan
          String query = "select * from naamakerroin"; // haku komento jolla haetaan kaikki naamakertoimet
          result = statement.executeQuery(query); // suoritetaan komento
          while(result.next()){ // otetaan talteen naamakertoimesta saadut tiedot
              int oppilaan_id = result.getInt("oppilas_id");
              int hymiön_id = result.getInt("hymiö_id");
              naamakerroin = new Naamakerroin(oppilaan_id, hymiön_id); // luodaan uusi naamakerroin olio saaduilla tiedoilla
              naamakerroinLista.add(naamakerroin); // lisätään luotu naamakerroin olio naamakerroin listaan
          }
          return naamakerroinLista; // palautetaan naamakerroin lista
      } catch (Exception e) { // jos tapahtui virhe
          System.out.println("Virhe HaeKaikkiNaamakertoimet!");
          System.out.println(e);
          return null; // palautetaan null jos ei onnistunut
      }
  }
  
  // Metodilla voidaan muuttaa oppilaalle annettua naamakerrointa
  // Oppilas jonka naamakerrointa muutetaan tunnistetaaan parametrina saadulla oppilas_id:llä
  public boolean updateNaamakerroin(int oppilas_id, int hymiö_id){
      try {
          String query = "update naamakerroin set hymiö_id=" + hymiö_id + " where oppilas_id=" + oppilas_id; // komento jolla oppilaan naamakerroin muuttetaan
          statement.executeUpdate(query); // suoritetaan komento
          return true; // palautetaan true jos onnistui
      } catch (Exception e) { // virheen sattuessa
          System.out.println("Virhe updateNaamakerroin!");
          System.out.println(e);
          return false; // jos epäonnistui palautetaan false
      }
  }
  
  // Metodilla voidaan antaa arvosana oppilaalle tietystä kurssista
  // Oppilas tunnistetaan parametrina saatavalla oppilas_id:llä 
  // Kurssi tunnistetaan parametrinä saatavalla kurssin_id:llä
  public boolean createArvosana(int oppilas_id, int kurssin_id, int arvosana){
      try {
          String query = "insert into arvosana values(" + oppilas_id + ", " + kurssin_id + ", " + arvosana + ")"; // komento jolla luodaan uusi arvosana oppilaalle
          statement.executeUpdate(query); // suoritetaan komento
          return true; // palautetaan tosi jos onnistui
      } catch (Exception e) { // virheen sattuessa
          System.out.println("Virhe createArvosana!");
          System.out.println(e);
          return false; // palautetaan false jos epäonnistui
      }
  }
  
  // Metodilla voidaan päivittää oppilaan arvosanaa tietyllä kurssilla
  // Oppilas tunnistetaan parametrinä saatavalla oppilas_id:llä
  // Kurssi tunnistetaan parametrinä saaatavalla kurssin_id:llä
  public boolean updateArvosana(int oppilas_id, int kurssin_id, int arvosana){
      try {
          System.out.println("Aloitetaan arvosanan luonti!");
          String query = "update arvosana set arvosana=" + arvosana + " where oppilas_id=" + oppilas_id + " and kurssin_id=" + kurssin_id; // komento jolla oppilaan arvosana kurssilta päivitetään
          statement.executeUpdate(query); // suoritetaan komento
          return true; // palautetaan tosi jos onnistui
      } catch (Exception e) { // virheen sattuessa
          System.out.println("Virhe updateArvosana!");
          System.out.println(e);
          return false; // palautetaan false jos epäonnistui
      }
  }
  
  private Arvosana arvosana;
  
  
  // Metodi hakee yhden oppilaan arvosanan tietyltä kurssilta
  // Kurssi ja oppilas tunnistetaan parametrinä saaduilla id:illä
  public Arvosana HaeArvosana(int oppilas_id, int kurssin_id){
      System.out.println("Aloitetaan arvosanan luonti!");
      try {
          String query = "select * from arvosana where oppilas_id=" + oppilas_id + " and kurssin_id=" + kurssin_id; // komento jolla haetaan oppilaan arvosana tietyltä kurssilta
          result = statement.executeQuery(query);// suoritetaan hakukomento
          while(result.next()){ // niin kauan kuin tuloksia on jäljellä
              // poimitaan tiedot talteen
              int oppilaan_id = result.getInt("oppilas_id");
              int kurssinId = result.getInt("kurssin_id");
              int arvoSana = result.getInt("arvosana");
              arvosana = new Arvosana(oppilaan_id, kurssinId, arvoSana); // luodaan uusi arvosana olio saaduilla arvoilla
              System.out.println("Arvosana luotu!");
          }
          return arvosana;
      } catch (Exception e) {
          System.out.println("Virhe HaeArvosana(oppilas_id, kurssin_id)");
          System.out.println(e);
          return null; // jos tapahtui virhe palautetaan null
      }
  }
  // Metodi hakee kaikkien oppilaiden arvosanat tietyltä kurssilta
  // Kurssi tunnistetaan parametrinä saadulla kurssin_id:llä
  public ArrayList<Arvosana> HaeArvosana(int kurssin_id){
      try {
          ArrayList<Arvosana> arvosanaLista = new ArrayList(); // luodaan arvosana lista jonne arvosana oliot tallennetaan
          String query = "select * from arvosana where kurssin_id=" + kurssin_id; // hakukomento jolla haetaan koko kurssin arvosanat
          result = statement.executeQuery(query); // suoritetaan hakukomento
          while(result.next()){ // niin kauan kuin löytyy arvosanoja
              // poimitaan tiedot talteen
              int oppilas_id = result.getInt("oppilas_id");
              int kurssinId = result.getInt("kurssin_id");
              int arvoSana = result.getInt("arvosana");
              arvosana = new Arvosana(oppilas_id, kurssinId, arvoSana); // luodaan uusi arvosana olio saaduilla tiedoilla
              arvosanaLista.add(arvosana); // lisätään luotu olio arvosana listaan
          }
          return arvosanaLista; // kun ollaan valmiita palautetaan löydet arvosanat
      } catch (Exception e) {
          System.out.println("Virhe HaeArvosana(kurssin_id)!");
          System.out.println(e);
          return null; // virheen sattuessa palautetaan null
      }
  }
  
  // Metodilla voidaan hakea kaikki oppilaan arvosanat
  // Oppilas tunnistetaan parametrillä saadulla oppilas_id:llä
  public ArrayList<Arvosana> HaeOppilaanKaikkiArvosanat(int oppilas_id){
      try {
          ArrayList<Arvosana> arvosanaLista = new ArrayList(); // luodaan arvosanalista jonne tallennetaan saadut arvosanat
          String query = "select * from arvosana where oppilas_id=" + oppilas_id; // hakukomento jolla haetaan kaikki oppilaan arvosanat
          result = statement.executeQuery(query); // suoritetaan hakukomento
          while(result.next()){ // niin kauan kuin arvosanoja on jäljellä
              // poimitaan tiedot talteen
              int oppilaan_id = result.getInt("oppilas_id");
              int kurssin_id = result.getInt("kurssin_id");
              int arvoSana = result.getInt("arvosana");
              arvosana = new Arvosana(oppilaan_id, kurssin_id, arvoSana); // luodaan uusi arvosana olio saaduilla tiedoilla
              arvosanaLista.add(arvosana); // lisätään luotu olio arvosana listaan
          }
          return arvosanaLista; // kun ollaan valmiita palautetaan arvosanalista
      } catch (Exception e) {
          System.out.println("Virhe HaeOppilaanKaikkiArvosanat!");
          System.out.println(e);
          return null; // virheen sattuessa palautetaan null
      }
  }
  
  
   
 
    
}
    



