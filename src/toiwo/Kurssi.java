/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package toiwo;

/**
 *
 * @author Hannu
 */
public class Kurssi {
    
    private int kurssin_id;
    private String kurssinNimi;
    private String kurssinKuvaus;
    
    public Kurssi(){
        kurssin_id = 0;
        kurssinNimi = "";
        kurssinKuvaus = "";
    }
    
    public Kurssi(int kurssin_id, String kurssinNimi, String kurssinKuvaus){
        this.kurssin_id = kurssin_id;
        this.kurssinNimi = kurssinNimi;
        this.kurssinKuvaus = kurssinKuvaus;
    }

    public int getKurssin_id() {
        return kurssin_id;
    }

    public void setKurssin_id(int kurssin_id) {
        this.kurssin_id = kurssin_id;
    }

    public String getKurssinNimi() {
        return kurssinNimi;
    }

    public void setKurssinNimi(String kurssinNimi) {
        this.kurssinNimi = kurssinNimi;
    }

    public String getKurssinKuvaus() {
        return kurssinKuvaus;
    }

    public void setKurssinKuvaus(String kurssinKuvaus) {
        this.kurssinKuvaus = kurssinKuvaus;
    }
    
    
    
}
