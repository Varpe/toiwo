/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package toiwo;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.VBoxBuilder;
import javafx.scene.text.Text;
import javafx.stage.Modality;
import javafx.stage.Stage;

/**
 *
 * @author Hannu
 */
public class DisplayConfirmation { // Luokka jossa on popup ikkunoiden toteutus
    
    
    // Metodi luo popup ikkunan ja esittää sen käyttöliittymässä
    // saa parametriksi boolean tila, true kertoo onnistumisesta, false kertoo epäonnistumisesta
    public void display(boolean tila, String missä){

    final Stage myDialog = new Stage(); // luodaa stage jossa popup näytetään
    myDialog.initModality(Modality.WINDOW_MODAL);
    Scene myDialogScene = null; // alustetaan tarvittava scene
    Button okButton = new Button("Sulje"); // luodaan sulje nappi 
    okButton.setOnAction(new EventHandler<ActionEvent>(){ // sulje nappia painessa suljetaan popup ikkuna
        @Override
        public void handle(ActionEvent arg0) {
            myDialog.close();
        }

    });
    
    if ("tallennaOppilas".equals(missä)){ // jos kyseessä on oppilaan tallennus
        if(tila == true){ // oppilaan tallennuksen onnistuessa
        myDialogScene = new Scene(VBoxBuilder.create()
            .children(new Text("Oppilas tallennettu."), okButton)
            .alignment(Pos.CENTER)
            .padding(new Insets(10))
            .build());
        }
    else{ // oppilaan tallennukse epäonnistuessa
        myDialogScene = new Scene(VBoxBuilder.create()
            .children(new Text("Virhe! Oppilasta ei voitu tallentaa."), okButton)
            .alignment(Pos.CENTER)
            .padding(new Insets(10))
            .build());
}
        
    }
    
    else if("tallennaKurssi".equals(missä)){ // jos kyseessä on kurssin tallennus
        if(tila == true){ // kurssin tallennuksen onnistuessa
        myDialogScene = new Scene(VBoxBuilder.create()
            .children(new Text("Kurssi tallennettu."), okButton)
            .alignment(Pos.CENTER)
            .padding(new Insets(10))
            .build());
}
    else{ // kurssin tallenuksen epäonnistuessa
        myDialogScene = new Scene(VBoxBuilder.create()
            .children(new Text("Virhe! Kurssia ei voitu tallentaa."), okButton)
            .alignment(Pos.CENTER)
            .padding(new Insets(10))
            .build());
}
    }
    else if("poistaOppilas".equals(missä)){ // jos kyseessä on oppilaan poisto
        if(tila == true){ // oppilaan poiston onnistuessa
        myDialogScene = new Scene(VBoxBuilder.create()
            .children(new Text("Oppilas poistettu."), okButton)
            .alignment(Pos.CENTER)
            .padding(new Insets(10))
            .build());
}
    else{ // oopilaan poiston epäonnistuessa
        myDialogScene = new Scene(VBoxBuilder.create()
            .children(new Text("Virhe! Oppilasta ei voitu poistaa."), okButton)
            .alignment(Pos.CENTER)
            .padding(new Insets(10))
            .build());
}
    }
    else if("poistaKurssi".equals(missä)){ // jos kyseessä on kurssin poistaminen
        if(tila == true){ // kurssin poiston onnistuessa
        myDialogScene = new Scene(VBoxBuilder.create()
            .children(new Text("Kurssi poistettu."), okButton)
            .alignment(Pos.CENTER)
            .padding(new Insets(10))
            .build());
}
       else{ // kurssin poiston epäonnistuessa
        myDialogScene = new Scene(VBoxBuilder.create()
            .children(new Text("Virhe! Kurssia ei voitu poistaa!"), okButton)
            .alignment(Pos.CENTER)
            .padding(new Insets(10))
            .build());
}
    }
        
        else if("lisääOppilasKurssi".equals(missä)){ // jos kyseessä on oppilaan lisääminen kurssille
        if(tila == true){ // kurssin poiston onnistuessa
        myDialogScene = new Scene(VBoxBuilder.create()
            .children(new Text("Oppilas lisätty kurssille."), okButton)
            .alignment(Pos.CENTER)
            .padding(new Insets(10))
            .build());
}
    else{ // jos oppilaan lisääminen kurssille epäonnistui
        myDialogScene = new Scene(VBoxBuilder.create()
            .children(new Text("Virhe! Oppilasta ei voitu lisätä kurssille."), okButton)
            .alignment(Pos.CENTER)
            .padding(new Insets(10))
            .build());
}
    }
    
    else if("poistaOppilasKurssilta".equals(missä)){ // jos kyseessä on kurssin poistaminen
        if(tila == true){ // kurssin poiston onnistuessa
        myDialogScene = new Scene(VBoxBuilder.create()
            .children(new Text("Oppilas poistettu kurssilta."), okButton)
            .alignment(Pos.CENTER)
            .padding(new Insets(10))
            .build());
}
       else{ // kurssin poiston epäonnistuessa
        myDialogScene = new Scene(VBoxBuilder.create()
            .children(new Text("Virhe! Oppilasta ei voitu poistaa kurssilta!"), okButton)
            .alignment(Pos.CENTER)
            .padding(new Insets(10))
            .build());
}
    }
    myDialog.setScene(myDialogScene); // annetaan luodulle stagelle scene
    myDialog.show(); // näytetään popup ikkuna
}



}
