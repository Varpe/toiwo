/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package toiwo;

/**
 *
 * @author Hannu
 */
public class OppilasKurssi {
    
    private int oppilas_id;
    private int kurssin_id;
    
    public OppilasKurssi(){
        oppilas_id = 0;
        kurssin_id = 0;
    }
    
    public OppilasKurssi(int oppilas_id, int kurssin_id){
        this.oppilas_id = oppilas_id;
        this.kurssin_id = kurssin_id;
    }
    
    public int getOppilas_id(){
        return oppilas_id;
    }
    
    public int getKurssin_id(){
        return kurssin_id;
    }
    
    public void setOppilas_id(int oppilas_id){
        this.oppilas_id = oppilas_id;
    }
    
    public void setKurssin_id(int kurssin_id){
        this.kurssin_id = kurssin_id;
    }
    
}
