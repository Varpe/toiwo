/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package toiwo;

/**
 *
 * @author Hannu
 */
public class OppilasArvosana {
    
    private int oppilas_id;
    private int kurssin_id;
    private int suoritukset;
    private int arvosana;
    private String etunimi;
    private String sukunimi;
    
    public OppilasArvosana(){
        oppilas_id = 0;
        kurssin_id = 0;
        suoritukset = 0;
        arvosana = 0;
        etunimi = "";
        sukunimi = "";
    }
    
    public OppilasArvosana(int oppilas_id, int kurssin_id, int suoritukset, int arvosana, String etunimi, String sukunimi){
        this.oppilas_id = oppilas_id;
        this.kurssin_id = kurssin_id;
        this.suoritukset = suoritukset;
        this.arvosana = arvosana;
        this.etunimi = etunimi;
        this.sukunimi = sukunimi;
    }

    public int getOppilas_id() {
        return oppilas_id;
    }

    public void setOppilas_id(int oppilas_id) {
        this.oppilas_id = oppilas_id;
    }

    public int getKurssin_id() {
        return kurssin_id;
    }

    public void setKurssin_id(int kurssin_id) {
        this.kurssin_id = kurssin_id;
    }

    public int getSuoritukset() {
        return suoritukset;
    }

    public void setSuoritukset(int suoritukset) {
        this.suoritukset = suoritukset;
    }

    public int getArvosana() {
        return arvosana;
    }

    public void setArvosana(int arvosana) {
        this.arvosana = arvosana;
    }

    public String getEtunimi() {
        return etunimi;
    }

    public void setEtunimi(String etunimi) {
        this.etunimi = etunimi;
    }

    public String getSukunimi() {
        return sukunimi;
    }

    public void setSukunimi(String sukunimi) {
        this.sukunimi = sukunimi;
    }
    
    
    
}
