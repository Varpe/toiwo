/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package toiwo;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.scene.image.Image;
import javax.imageio.ImageIO;

/**
 *
 * @author Varpe
 */
public class Grafiikka {
    
    public Image LataaKuva(int hymiö_id){
        
        Image iloinen = new Image(getClass().getResourceAsStream("iloinen.jpg"));
        Image perus = new Image(getClass().getResourceAsStream("perus.jpg"));
        Image suru = new Image(getClass().getResourceAsStream("suru.jpg"));
        List<Image> images = new ArrayList<>();
        images.add(perus);
        images.add(suru);
        images.add(iloinen);
        return images.get(hymiö_id);
    }
    
}
