/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package toiwo;

import java.util.ArrayList;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Hannu
 */
public class DataBaseConnectorTest {
    
    public DataBaseConnectorTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of Connect method, of class DataBaseConnector.
     */
    @Test
    public void testConnect() {
        System.out.println("Connect");
        DataBaseConnector instance = new DataBaseConnector();
        boolean expResult = true;
        boolean result = instance.Connect();
        assertEquals(expResult, result);
    }

    /**
     * Test of createKurssi method, of class DataBaseConnector.
     */
    @Test
    public void testCreateKurssi() {
        System.out.println("createKurssi");
        String kurssinNimi = "JunitTesti";
        int kurssinId = 111;
        String kuvaus = "Tämä on Junit testi.";
        DataBaseConnector instance = new DataBaseConnector();
        instance.Connect();
        boolean expResult = true;
        boolean result = instance.createKurssi(kurssinNimi, kurssinId, kuvaus);
        assertEquals(expResult, result);
    }

    /**
     * Test of createOppilas method, of class DataBaseConnector.
     */
    @Test
    public void testCreateOppilas() {
        System.out.println("createOppilas");
        int oppilas_id = 111;
        String etunimi = "Junit";
        String sukunimi = "Testi";
        int suoritukset = 0;
        DataBaseConnector instance = new DataBaseConnector();
        instance.Connect();
        boolean expResult = true;
        boolean result = instance.createOppilas(oppilas_id, etunimi, sukunimi, suoritukset);
        assertEquals(expResult, result);
    }

    /**
     * Test of haeKaikkiOppilaat method, of class DataBaseConnector.
     */
    /*
    @Test
    public void testHaeKaikkiOppilaat() {
        System.out.println("haeKaikkiOppilaat");
        DataBaseConnector instance = new DataBaseConnector();
        instance.Connect();
        ArrayList<Oppilas> expResult = null;
        ArrayList<Oppilas> result = instance.haeKaikkiOppilaat();
        assertEquals(expResult, result);
    }*/

    /**
     * Test of haeKaikkiKurssit method, of class DataBaseConnector.
     */
    /*
    @Test
    public void testHaeKaikkiKurssit() {
        System.out.println("haeKaikkiKurssit");
        DataBaseConnector instance = new DataBaseConnector();
        ArrayList<Kurssi> expResult = null;
        ArrayList<Kurssi> result = instance.haeKaikkiKurssit();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }*/

    /**
     * Test of poistaOppilas method, of class DataBaseConnector.
     */
    @Test
    public void testPoistaOppilas() {
        System.out.println("poistaOppilas");
        int oppilas_id = 111;
        DataBaseConnector instance = new DataBaseConnector();
        instance.Connect();
        boolean expResult = true;
        boolean result = instance.poistaOppilas(oppilas_id);
        assertEquals(expResult, result);
    }

    /**
     * Test of poistaKurssi method, of class DataBaseConnector.
     */
    @Test
    public void testPoistaKurssi() {
        System.out.println("poistaKurssi");
        int kurssin_id = 111;
        DataBaseConnector instance = new DataBaseConnector();
        instance.Connect();
        boolean expResult = true;
        boolean result = instance.poistaKurssi(kurssin_id);
        assertEquals(expResult, result);
    }

    /**
     * Test of updateOppilas method, of class DataBaseConnector.
     */
    @Test
    public void testUpdateOppilas() {
        System.out.println("updateOppilas");
        int oppilas_id = 0;
        String etunimi = "junit";
        String sukunimi = "testi";
        int suoritukset = 0;
        DataBaseConnector instance = new DataBaseConnector();
        instance.Connect();
        boolean expResult = true;
        boolean result = instance.updateOppilas(oppilas_id, etunimi, sukunimi, suoritukset);
        assertEquals(expResult, result);
    }

    /**
     * Test of updateKurssi method, of class DataBaseConnector.
     */
    @Test
    public void testUpdateKurssi() {
        System.out.println("updateKurssi");
        int kurssin_id = 0;
        String kurssinNimi = "junit";
        String kurssinKuvaus = "testi";
        DataBaseConnector instance = new DataBaseConnector();
        instance.Connect();
        boolean expResult = true;
        boolean result = instance.updateKurssi(kurssin_id, kurssinNimi, kurssinKuvaus);
        assertEquals(expResult, result);
    }
    
}
