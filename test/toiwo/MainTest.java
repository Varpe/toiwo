/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package toiwo;

import javafx.stage.Stage;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Varpe
 */
public class MainTest {
    
    public MainTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of lisaaOppilasKurssille method, of class Main.
     */
    @Test
    public void testLisaaOppilasKurssille() {
        System.out.println("lisaaOppilasKurssille");
        int kurssiID = 0;
        Main instance = new Main();
        instance.lisaaOppilasKurssille(kurssiID);
    }

    /**
     * Test of muokkaaOppilas method, of class Main.
     */
    @Test
    public void testMuokkaaOppilas() {
        System.out.println("muokkaaOppilas");
        String etu = "";
        String suku = "";
        int id = 0;
        Main instance = new Main();
        instance.muokkaaOppilas(etu, suku, id);
    }

    /**
     * Test of muokkaaKurssi method, of class Main.
     */
    @Test
    public void testMuokkaaKurssi() {
        System.out.println("muokkaaKurssi");
        String nimi = "";
        String kuva = "";
        int kID = 0;
        Main instance = new Main();
        instance.muokkaaKurssi(nimi, kuva, kID);
    }

    /**
     * Test of lisaaOpiskelija method, of class Main.
     */
    @Test
    public void testLisaaOpiskelija() {
        System.out.println("lisaaOpiskelija");
        Main instance = new Main();
        instance.lisaaOpiskelija();
    }

    /**
     * Test of lisaaKurssi method, of class Main.
     */
    @Test
    public void testLisaaKurssi() {
        System.out.println("lisaaKurssi");
        Main instance = new Main();
        instance.lisaaKurssi();
    }

    /**
     * Test of naytaOppilaat method, of class Main.
     */
    @Test
    public void testNaytaOppilaat() {
        System.out.println("naytaOppilaat");
        Main instance = new Main();
        instance.naytaOppilaat();
    }

    /**
     * Test of naytaKurssit method, of class Main.
     */
    @Test
    public void testNaytaKurssit() {
        System.out.println("naytaKurssit");
        Main instance = new Main();
        instance.naytaKurssit();
    }

    /**
     * Test of start method, of class Main.
     */
    @Test
    public void testStart() throws Exception {
        System.out.println("start");
        Stage primaryStage = null;
        Main instance = new Main();
        instance.start(primaryStage);
    }

    /**
     * Test of main method, of class Main.
     */
    @Test
    public void testMain() {
        System.out.println("main");
        String[] args = null;
        Main.main(args);
    }

    /**
     * Test of init method, of class Main.
     */
    @Test
    public void testInit() {
        System.out.println("init");
        Main instance = new Main();
        instance.init();
    }
    
}
